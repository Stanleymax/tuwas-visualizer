# Tuwas Visualizer for DHIS2

This project is was started during the Inf5750 - Open source software development course at the University of Oslo
The purpose of the app is to make it easy for users to extract data from DHIS2 and display this data in a user friendly way.

The District Health Information System (DHIS) is a highly flexible, open-source health management information system
and data warehouse. It is developed by the Health Information Systems Programme (HISP). The core development
activities are managed and coordinated by the Department of Informatics at the University of Oslo, and supported by
The Norwegian Research Council, NORAD, The University of Oslo, and The Norwegian Centre for International Cooperation in Education.

For the first version Tuwas Visualizer will support line, bar and pie charts. And the user will be able to choose from
some predefined settings.

### Technologies

The project is build using AngularJS. We use several angular plugins for ui and graphs. Most of the dependencies are managed
 using bower.

_Note: While angular is client-side-only technology and it's possible to create angular webapps that
don't require a backend server at all, we recommend hosting the project files using a local
webserver during development to avoid issues with security restrictions (sandbox) in browsers. The
sandbox implementation varies between browsers, but quite often prevents things like cookies, xhr,
etc to function properly when an html page is opened via `file://` scheme instead of `http://`._


### Running the app during development

You can pick one of these options:

* serve this repository with your webserver
* install node.js and run `scripts/web-server.js`

Then navigate your browser to `http://localhost:<port>/app/index.html` to see the app running in
your browser.


### Running the app in production

Tuwas Visualizer is an Angular app. Angular apps are really just a bunch of static html, css and
js files that just need to be hosted somewhere, where they can be accessed by browsers.


### Running unit tests

We have used [jasmine](http://pivotal.github.com/jasmine/) and
[Karma](http://karma-runner.github.io) for our unit tests/specs.

Requires [node.js](http://nodejs.org/), Karma (`sudo npm install -g karma`) and a local
or remote browser.

* start `scripts/test.sh` (on windows: `scripts\test.bat`)
  * a browser will start and connect to the Karma server (Chrome is default browser, others can be captured by loading the same url as the one in Chrome or by changing the `config/karma.conf.js` file)
* to run or re-run tests just change any of your source or test javascript files


### End to end testing

Angular ships with a baked-in end-to-end test runner that understands angular, your app and allows
you to write your tests with jasmine-like BDD syntax.

Requires a webserver, node.js + `./scripts/web-server.js` or your backend server that hosts the angular static files.

_Note: We have also added `./server.sh` that runs the same script but can be called with less characters :)

Check out the
[end-to-end runner's documentation](http://docs.angularjs.org/guide/dev_guide.e2e-testing) for more
info.

* create your end-to-end tests in `test/e2e/scenarios.js`
* serve your project directory with your http/backend server or node.js + `scripts/web-server.js`
* to run do one of:
  * open `http://localhost:port/test/e2e/runner.html` in your browser
  * run the tests from console with [Karma](http://karma-runner.github.io) via
    `scripts/e2e-test.sh` or `script/e2e-test.bat`


## Directory Layout

    app/                --> all of the files to be used in production
      css/              --> css files
        app.css         --> default stylesheet
        bootstrap.min.css           --> Bootstrap css
        bootstrap-theme.min.css     --> Not currently used
        sticky-footer-navbar.css    --> Not currently used
      img/              --> image files
        glyphicons-halflings.png        --> Glyphicon icons
        glyphicons-halflings-white.png  --> Gphyphicon icons in white
      index.html        --> app layout file (the main html template file of the app)
      index-async.html  --> just like index.html, but loads js files asynchronously
      js/               --> javascript files
        app.js          --> application
        controllers/    --> application controllers
            BarGraphController.js       --> Controller for bar graphs
            LinearGraphController.js    --> Controller for linear graphs
            PieChartController.js       --> Controller for pie graphs
        directives.js   --> application directives
        filters.js      --> custom angular filters
        services.js     --> custom angular services
      lib/              --> angular and 3rd party javascript libraries
        angular/
          angular.js        --> the latest angular js
          angular.min.js    --> the latest minified angular js
          angular-*.js      --> angular add-on modules
          version.txt       --> version number
        bower/              --> Bower components folder
      partials/             --> angular view partials (partial html templates)
        BarGraphPartial.html
        LinearGraphPartial.html
        PieChartPartial

    config/karma.conf.js        --> config file for running unit tests with Karma
    config/karma-e2e.conf.js    --> config file for running e2e tests with Karma

    scripts/            --> handy shell/js/ruby scripts
      e2e-test.sh       --> runs end-to-end tests with Karma (*nix)
      e2e-test.bat      --> runs end-to-end tests with Karma (windows)
      test.bat          --> autotests unit tests with Karma (windows)
      test.sh           --> autotests unit tests with Karma (*nix)
      web-server.js     --> simple development webserver based on node.js

    test/               --> test source files and libraries
      e2e/              -->
        runner.html     --> end-to-end test runner (open in your browser to run)
        scenarios.js    --> end-to-end specs
      fixtures/
        indicatorgroups.json    --> fixture for a json response from dhis2 api
        indicator.json          --> fixture for a json response from dhis2 api
      lib/
        angular/                --> angular testing libraries
          angular-mocks.js      --> mocks that replace certain angular services in tests
          angular-scenario.js   --> angular's scenario (end-to-end) test runner library
          version.txt           --> version file
      unit/                     --> unit level specs/tests
        controllersSpec.js      --> specs for controllers
        directivessSpec.js      --> specs for directives
        filtersSpec.js          --> specs for filters
        servicesSpec.js         --> specs for services
    bower.json --> Bower config file that holds dependencies
    server.sh  --> Run the webserver script in the script/ directory.

## Contact

For more information on Tuwas Visualizer please contact any of our team members.

For more information on DHIS2 please visit `http://www.dhis2.org`

For more information on AngularJS please check out `http://angularjs.org/`
