'use strict';

/* jasmine specs for services go here */

describe('service', function () {
    var baseUrl = 'http://apps.dhis2.org/demo/api/';
    var result = undefined;
    var response = function (response_obj) {
        result = response_obj;
    };

    beforeEach(module('TuwasApp.services'));


    describe('version', function () {
        it('should return current version', inject(function (version) {
            expect(version).toEqual('0.1');
        }));
    });

    describe('DHIS Webservice', function () {
        var $httpBackend;

        jasmine.getJSONFixtures().fixturesPath='base/test/fixtures';

        beforeEach(inject(function($injector){
            $httpBackend = $injector.get('$httpBackend');
        }));

        describe('IndicatorGroups', function () {

            beforeEach(function(){
                $httpBackend.whenJSONP(baseUrl + 'indicatorGroups.jsonp?callback=JSON_CALLBACK')
                    .respond(200, getJSONFixture('indicatorgroups.json'));
                $httpBackend.expectJSONP(baseUrl + 'indicatorGroups.jsonp?callback=JSON_CALLBACK');
            });

            it('should have objects with a name and id', inject(function (IndicatorGroups, $httpBackend) {
                IndicatorGroups.get(response);
                $httpBackend.flush();

                expect(result.indicatorGroups[0].name).toBe('ANC');
                expect(result.indicatorGroups[0].id).toBe('oehv9EO3vP7');
            }));

            it('should return an objects with an array of data', inject(function (IndicatorGroups, $httpBackend) {
                IndicatorGroups.get(response);
                $httpBackend.flush();

                expect(typeof result).toEqual('object');
                expect(result.indicatorGroups.length).toEqual(13);
            }));

        });

        describe('Indicators', function () {

            beforeEach(function () {
                $httpBackend.whenJSONP(baseUrl + 'indicators.jsonp?callback=JSON_CALLBACK')
                    .respond(200, getJSONFixture('indicators.json'));
                $httpBackend.expectJSONP(baseUrl + 'indicators.jsonp?callback=JSON_CALLBACK');
            });

            it('should return an object with an array of data', inject(function(Indicators){
                Indicators.get(response);
                $httpBackend.flush();

                expect(typeof result).toEqual('object');
                expect(result.indicators.length).toEqual(44);
            }));

            it('should have objects with a name and id', inject(function (Indicators){
                Indicators.get(response);
                $httpBackend.flush();

                expect(result.indicators[0].name).toBe('ANC 1 Coverage');
                expect(result.indicators[0].id).toBe('Uvn6LCg7dVU');
            }));

        });

        describe('Analytics', function () {

            beforeEach(function () {
                $httpBackend.whenJSONP(baseUrl + 'analytics.jsonp?dimension=dx:Uvn6LCg7dVU&dimension=pe:LAST_12_MONTHS&filter=ou:ImspTQPwCqd&callback=JSON_CALLBACK')
                    .respond(200, getJSONFixture('analytics.json'));
                $httpBackend.expectJSONP(baseUrl + 'analytics.jsonp?dimension=dx:Uvn6LCg7dVU&dimension=pe:LAST_12_MONTHS&filter=ou:ImspTQPwCqd&callback=JSON_CALLBACK')
            });
        });
    });
});
