'use strict';

angular.module('TuwasApp.filters', [])
    /**
     * Capitalize the first letter of the input
     *
     * @Param input {String} Text that will be processed
     */
    .filter('capitalize', function () {
        return function (input) {
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        }
    })
    /**
     * Converts all characters to lower case and replaces underscores with spaces
     *
     * @param input {String} Text to process
     * TODO: Naming of this method is not really relevant to time intervals. Perhaps a more general name is suitable
     */
    .filter('timeinterval', function () {
        return function (input, scope) {
            return input.toLowerCase().replace(new RegExp("_", "g"), ' ');
        }
    })
;