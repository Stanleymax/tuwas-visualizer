angular.module('TuwasApp.services.charts')
  .factory('PieChart', function (DataElementChart, DataElementGroups, OrganisationUnitGroupSets, Analytics, Utils) {
    var pieChart = DataElementChart(),
      organizationGroups = [],
      currentOrganizationGroup = null,
      organizationGroupUnits = [],
      pieChart;

    function loadOrganizationUnits() {
      //Check for a valid dataOrgGroup
      if (Utils.isValidId(currentOrganizationGroup.id)) {
        //Get a list of Org units that belong to the selected group
        OrganisationUnitGroupSets.get(currentOrganizationGroup.id).then(function (data) {
          pieChart.setOrganizationGroupUnits(data.items);
          if (pieChart.getCurrentDataElement() != null) {
            pieChart.updateData();
          }
        }, function (response) {
          //TODO display error message
        });
      }
    }

    function call_analytics() {
      Analytics.reset();

      Analytics.addDimension(pieChart.getCurrentOrganizationGroup().id, pieChart.getOrganizationGroupUnits());
      Analytics.addDimension(pieChart.getCurrentDataElement().id, 'dx');
      Analytics.addFilter(pieChart.getTimePeriod(), 'pe');

      Analytics.getList().then(function (data) {
        pieChart.setGraphData(data);
        pieChart.getDrawCallback()(data);
      }, function () {
        //TODO Do some error handling
      });
    }

    pieChart.init = function (callback) {
      pieChart.setDrawCallback(callback);

      if (this.getDataElementGroups().length === 0) {
        DataElementGroups.getList().then(function (response) {
          pieChart.setDataElementGroups(response.dataElementGroups);
        });
      }

      if (this.getOrganizationGroups().length === 0) {
        OrganisationUnitGroupSets.getList().then(function (response) {
          pieChart.setOrganizationGroups(response.dimensions);
        });
      }
    }

    pieChart.updateData = function () {
      var dataOrgGroup = pieChart.getCurrentOrganizationGroup();

      if (dataOrgGroup != null) { //TODO null checks make me sad :( change this to check for some sort of value
        if (Utils.isValidId(this.getCurrentDataElement().id)) {
          call_analytics();
        }
      }
    }
    pieChart.setOrganizationGroups = function (og) {
      organizationGroups = og;
    }
    pieChart.getOrganizationGroups = function () {
      return organizationGroups;
    }
    pieChart.setCurrentOrganizationGroup = function (cog) {
      currentOrganizationGroup = cog;

      loadOrganizationUnits();
    }
    pieChart.getCurrentOrganizationGroup = function () {
      return currentOrganizationGroup;
    }
    pieChart.setOrganizationGroupUnits = function (ogu) {
      organizationGroupUnits = ogu;
    }
    pieChart.getOrganizationGroupUnits = function () {
      return organizationGroupUnits;
    }


    return pieChart;
  });
