"use strict";

angular.module('TuwasApp.services.charts', [])
  .factory('Chart', function (SelectedTimeIntervals, $log) {
    return function () {
      var graph_data = false,
        time_index = 1,
        getGraphData = function () {
          return graph_data;
        },
        setGraphData = function (graphData) {
          graph_data = graphData;
        },
        setDrawCallback = function (callback) {
          this.draw_callback = callback;
        },
        getDrawCallback = function () {
          if (typeof this.draw_callback === 'function')
            return this.draw_callback;
          return function () {
          };
        },
        setPreviousTimePeriod = function () {
          // If we have an empty time array keep the index on zero
          if (this.time.length === 0) {
            time_index = 0;
            return;
          }

          if (time_index <= 0) {
            time_index = this.time.length - 1;
          } else {
            time_index -= 1;
          }
        },
        setNextTimePeriod = function () {
          if (time_index >= this.time.length - 1) {
            time_index = 0;
          } else {
            time_index += 1;
          }
        },
        getTimePeriod = function () {
          if (typeof this.time[time_index] === 'undefined') {
            throw {
              name: 'GraphError',
              message: 'There are currently no selected time periods, please select some from the settings menu.'
            };
          }
          return this.time[time_index];
        },
        updateDataLeft = function () {
          this.setPreviousTimePeriod();
          this.updateData();
        },
        updateDataRight = function () {
          this.setNextTimePeriod();
          this.updateData();
        }

      return {
        getDrawCallback: getDrawCallback,
        setDrawCallback: setDrawCallback,
        isManualVisible: true,
        getGraphData: getGraphData,
        setGraphData: setGraphData,
        time: SelectedTimeIntervals,
        setNextTimePeriod: setNextTimePeriod,
        setPreviousTimePeriod: setPreviousTimePeriod,
        getTimePeriod: getTimePeriod,
        updateDataLeft: updateDataLeft,
        updateDataRight: updateDataRight
      };
    }
  })
  .factory('DataElementChart', function (Chart, DataElementGroups, Utils) {
    return function () {
      var dataElementChart = Chart(),
        dataElementGroups = [],
        dataElementGroup = {},
        availableDataElements = [],
        dataElement = {};

      function loadDataElements() {
        DataElementGroups.get(dataElementGroup.id).then(function (data) {
          dataElementChart.setDataElements(data.dataElements);
        }, function (response) {
          //TODO Display error message
        });
      }

      dataElementChart.init = function (callback) {
        dataElementChart.setDrawCallback(callback);

        if (dataElementGroups.length === 0) {
          //Call DataElementGroup service to get a list of DataElementGroups and execute the then function when it's gotten it
          DataElementGroups.getList().then(function (response) {
            dataElementChart.setDataElementGroups(response.dataElementGroups);
          });
        }
      }

      dataElementChart.getDataElementGroups = function () {
        return dataElementGroups
      }
      dataElementChart.setDataElementGroups = function (degs) {
        dataElementGroups = degs;
      }
      dataElementChart.getCurrentDataElementGroup = function () {
        return dataElementGroup;
      }
      dataElementChart.setCurrentDataElementGroup = function (deg) {
        dataElementGroup = deg;
        dataElement = undefined;

        if (Utils.isValidId(deg.id)) {
          loadDataElements();
        }
      }
      dataElementChart.getDataElements = function () {
        return availableDataElements;
      }
      dataElementChart.setDataElements = function (des) {
        availableDataElements = des;
      }
      dataElementChart.getCurrentDataElement = function () {
        return dataElement;
      }
      dataElementChart.setCurrentDataElement = function (de) {
        dataElement = de;
      }

      return dataElementChart;
    }
  });

