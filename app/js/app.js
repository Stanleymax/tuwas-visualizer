'use strict';

/**
 * Declare app level module.
 */
var TuwasApp = angular.module('TuwasApp', [
    'ngResource',
    'ngRoute',
    'TuwasApp.services',
    'TuwasApp.services.charts',
    'TuwasApp.controllers',
    'TuwasApp.filters',
    'TuwasApp.utils',
    'ui.bootstrap',
    'nvd3ChartDirectives',
    'restangular',
    'ngTouch'
  ]).
  config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/bar', {templateUrl: 'partials/BarGraphPartial.html', controller: 'BarGraphController'});
    $routeProvider.when('/linear', {templateUrl: 'partials/LinearGraphPartial.html', controller: 'LinearGraphController'});
    $routeProvider.when('/pie', {templateUrl: 'partials/PieChartPartial.html', controller: 'PieChartController'});
    $routeProvider.when('/settings', {templateUrl: 'partials/SettingsPartial.html', controller: 'SettingsController'});
    $routeProvider.otherwise({redirectTo: '/linear'});
  }]);

/**
 * Angular run method that gets the manifest.webapp location
 * for our application
 */
TuwasApp.run(function ($http, $rootScope, Restangular) {
  /**
   * Get the location path of the manifest
   *
   * @returns {string} URI of the manifest.webapp
   */
  function getManifestPath() {
    var path = window.location.pathname.split('/');
    path.pop(); //Removes index.html
    path.pop(); //Removes app folder
    path = path.join('/');
    return 'http://' + window.location.host + path + '/manifest.webapp';
  }

  function initRestangular(path) {
    Restangular.setBaseUrl(path + '/api');
    Restangular.setDefaultHttpFields({cache: true});
  }

  /**
   * Make a http call to the server to retrieve the manifest and
   * save it to the angular root scope.
   */
  $http.get(getManifestPath()).success(function (manifest) {
    $rootScope.manifest = manifest;
    initRestangular(manifest.activities.dhis.href);
  }).error(function () {
    $rootScope.manifest = [];
  });
});
