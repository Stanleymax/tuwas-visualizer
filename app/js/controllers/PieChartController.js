'use strict';

angular.module('TuwasApp.controllers').
  controller('PieChartController', ['$scope', 'PieChart', 'Toggle',
    function ($scope, PieChart, Toggle) {
      $scope.settings.lastChart = 'pie';
      $scope.pieChart = PieChart;
      PieChart.init(mapGraphData);

      // Check if we have cached graph data and display it if we do
      if (PieChart.getGraphData()) {
        mapGraphData(PieChart.getGraphData());
      }

      $scope.pickElement = Toggle();
      $scope.pickElementGroup = Toggle();
      $scope.pickOrganizationGroup = Toggle();

      //For the swipe at left direction
      $scope.previousTime = function () {
        try {
          PieChart.updateDataLeft();
        } catch(error) {
          $scope.messages.addMessage(error.message, 'error');
        }
      }
      //For the swipe at right direction
      $scope.nextTime = function () {
        try {
          PieChart.updateDataRight();
        } catch(error) {
          $scope.messages.addMessage(error.message, 'error');
        }
      }

      $scope.updateDataElgroup = function (dataElementGroup) {
        PieChart.setCurrentDataElementGroup(dataElementGroup);

        $scope.pickElementGroup.toggle();
        $scope.pickElement.toggle(true);
      };

      $scope.updateDataElement = function (dataElement) {
        PieChart.setCurrentDataElement(dataElement);

        $scope.pickElement.toggle();

        if (PieChart.getCurrentOrganizationGroup() === null) {
          $scope.pickOrganizationGroup.toggle();
        }

        try {
          PieChart.updateData(mapGraphData);
        } catch(error) {
          $scope.messages.addMessage(error.message, 'error');
        }
      };

      $scope.updateDataOrgGroup = function (dataOrgGroup) {
        //Set the current dataOrgGroup onto the graph object
        PieChart.setCurrentOrganizationGroup(dataOrgGroup);

        $scope.pickOrganizationGroup.toggle();
      };

      function mapGraphData(data) {
        var values = [];

        if (data.rows[0] == null || data.rows[0] == undefined) {
          //Show the manual
          PieChart.isManualVisible = true;

          //Display a message to the user with the status
          $scope.messages.addMessage('There is no data for the selected values :(', 'warning');

          d3.select("#chart svg")
            .text("");
          return null;
        }

        //Hide the manual
        PieChart.isManualVisible = false;

        for (var i = 0; i < data.rows.length; i++) {

          var row = data.rows[i];
          var datas = row.toString().split(',');
          var name = datas[0];

          values.push({

            "label": data.metaData.names[name],
            "value": datas[2]
          });

        }
        redraw(values);
      }

      function redraw(graphData) {
        nv.addGraph(function () {

          var width = 500,
            height = 500;

          var chart = nv.models.pieChart()
            .x(function (d) {
              return d.label
            })
            .y(function (d) {
              return d.value
            })
            .color(d3.scale.category10().range());

          d3.select('#chart svg')
            .attr('width', 10)
            .attr('height', 10)
            .datum(graphData)
            .transition().duration(500)
            .call(chart);

          nv.utils.windowResize(chart.update);
        });
      }
    }]);
