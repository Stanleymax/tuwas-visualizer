'use strict';

angular.module('TuwasApp.controllers', [])
  .controller('AppController', ['$scope', '$location', function ($scope, $location) {
    $scope.messages = {
      message_queue: [],
      addMessage: function() {
        var message_id = 0,
            levels = {
              'success': 'alert-success',
              'error': 'alert-danger',
              'notice': 'alert-info',
              'warning': 'alert-warning'
            };

        return function (text, level) {
          var message = {
            id: message_id++,
            text: level + ': ' + text,
            level: levels[level]
          }
          this.message_queue.push(message);
        }
      }(),
      removeMessage: function (message_id) {
        this.message_queue.splice(message_id, 1);
      },
      clearMessages: function () {
        this.message_queue = [];
      }
    }

    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };

   $scope.info = {
      show: false,
      toggle: function () {
        $scope.info.show = ! $scope.info.show;
      }
    }

    $scope.menu = {
      show: true,
      toggle: function () {
        $scope.menu.show = ! $scope.menu.show;
      }
    }

    $scope.settings = {
      lastChart: ''
    }
  }]);
