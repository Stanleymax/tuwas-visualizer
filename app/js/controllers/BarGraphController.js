'use strict';

angular.module('TuwasApp.controllers').
  controller('BarGraphController', ['$scope', 'BarChart', 'Toggle',
    function ($scope, BarChart, Toggle) {
      $scope.settings.lastChart = 'bar';
      $scope.barChart = BarChart;
      BarChart.init(mapGraphData);

      // Check if we have cached graph data and display it if we do
      if (BarChart.getGraphData()) {
        mapGraphData(BarChart.getGraphData());
      }

      $scope.pickIndicatorGroup = Toggle();
      $scope.pickIndicator = Toggle();

      //For the swipe at left direction
      $scope.previousTime = function () {
        try {
          BarChart.updateDataLeft();
        } catch(error) {
          $scope.messages.addMessage(error.message, 'error');
        }
      }

      //For the swipe at right direction
      $scope.nextTime = function () {
        try{
          BarChart.updateDataRight();
        } catch(error) {
          $scope.messages.addMessage(error.message, 'error');
        }
      }

      $scope.setActiveIndicator = function (indicator) {
        BarChart.setCurrentIndicatorGroup(indicator);

        $scope.pickIndicatorGroup.toggle();
        $scope.pickIndicator.toggle();
      }

      $scope.updateDataElement = function (dataElement) {
        BarChart.setCurrentDataIndicator(dataElement);

        $scope.pickIndicator.toggle();

        try {
          BarChart.updateData();
        } catch(error) {
          $scope.messages.addMessage(error.message, 'error');
        }
      };

      function mapGraphData(data) {
        var key = BarChart.getCurrentDataIndicator().name;
        var values = [];
        if (data.rows[0] == null || data.rows[0] == undefined) {
          //Display a message to the user with the status
          $scope.messages.addMessage('There is no data for the selected values :(', 'warning');

          //Show the manual
          BarChart.isManualVisible = true;

          d3.select("#chart svg")
            .text("");
          return null;
        }

        //Hide the manual
        BarChart.isManualVisible = false;

        for (var i = 0; i < data.rows.length; i++) {
          var row = data.rows[i];
          var name = row[1];

          values.push({
            x: data.metaData.names[name],
            y: Math.abs(row[2])
          });
        }

        var graphData = [
          {
            "key": key,
            "values": values
          }
        ];

        redraw(graphData);
      };


      function redraw(graphData) {
        nv.addGraph(function () {
          var chart = nv.models.discreteBarChart()
            .x(function (d) {
              return d.x
            })
            .y(function (d) {
              return d.y
            })
            .color(d3.scale.category10().range());
          chart.xAxis.rotateLabels(-45);

          chart.margin({bottom: 80});
          // chart.forceY([ 0,3000]);
          d3.select('#chart svg')
            .attr('width', 13.5)
            .attr('height', 10)
            .datum(graphData)
            .transition().duration(500)
            .call(chart);

          nv.utils.windowResize(chart.update);

        });
      };
    }]);
